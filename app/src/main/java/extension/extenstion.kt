package extension

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false, context: Context): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}