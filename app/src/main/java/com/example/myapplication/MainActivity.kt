package com.example.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.view.Window
import android.view.WindowManager
import android.widget.Switch
import com.example.myapplication.arena.home.ArenaFragment
import com.example.myapplication.more.arena_more.MoreOptionFragment
import com.example.myapplication.myleague.league.MyLeagueFragment
import com.example.myapplication.profile.arena_profile.ProfileFragment

class MainActivity : AppCompatActivity() {

    private lateinit var toolbar: ActionBar
    private var arenaFragment: ArenaFragment? = null
    private var myLeagueFragment: MyLeagueFragment? = null
    private var profileFragment: ProfileFragment? = null
    private var moreOptionFragment: MoreOptionFragment? = null
    private var bottomNavigation: BottomNavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()
        toolbar = supportActionBar!!
        bottomNavigation = findViewById(R.id.navigation)
        this.bottomNavigation!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        initializeHomeFragment()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.arena -> {
                toolbar.setIcon(R.drawable.arena_pink)
                toolbar.title = "Arena"
                if (arenaFragment == null)
                    arenaFragment = ArenaFragment.newInstance()
                openFragment(arenaFragment!!)
                return@OnNavigationItemSelectedListener true
            }
            R.id.myleague -> {
                toolbar.setIcon(R.drawable.myleague_pink)
                toolbar.title = "My Leagus"
                if (myLeagueFragment == null)
                    myLeagueFragment = MyLeagueFragment.newInstance()
                openFragment(myLeagueFragment!!)
                return@OnNavigationItemSelectedListener true
            }
            R.id.profile -> {
                toolbar.setIcon(R.drawable.profile_pink)
                toolbar.title = "Me"
                if (profileFragment == null)
                    profileFragment = ProfileFragment.newInstance()
                openFragment(profileFragment!!)
                return@OnNavigationItemSelectedListener true
            }
            R.id.more -> {
                toolbar.setIcon(R.drawable.more_pink)
                toolbar.title = "More"
                if (moreOptionFragment == null)
                    moreOptionFragment = MoreOptionFragment.newInstance()
                openFragment(moreOptionFragment!!)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun initializeHomeFragment() {
        arenaFragment = ArenaFragment.newInstance()
        openFragment(arenaFragment!!)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.home_frame, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    public fun getMainActivity(): MainActivity {
        return this;
    }
}
