package com.example.myapplication.arena.home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import extension.inflate

class VideoListRecyclerAdapter(var context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return VideoListViewHolder(p0.inflate(p1, false, context))
    }

    override fun getItemCount(): Int {
        return 7
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.video_list_view_holder
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is VideoListRecyclerAdapter.VideoListViewHolder) p0.bindView()
    }

    class VideoListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        override fun onClick(v: View?) {

        }

        init {
            itemView.setOnClickListener(this)
        }

        fun bindView() {

        }
    }
}
