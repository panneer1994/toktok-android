package com.example.myapplication.arena.home

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.myapplication.R


class HomeViewPagerAdapter(var context: Context, fm: FragmentManager): FragmentStatePagerAdapter(fm)  {
    var tournmentFragment: TournmentFragment? = null
    var completedTournmentFragment: CompletedTournmentFragment? = null
    var videoListFragment: VideoListFragment? = null

    override fun getItem(p0: Int): Fragment {
        if (p0 == 0) {
            if (tournmentFragment == null)
                tournmentFragment = TournmentFragment()
            return tournmentFragment!!
        } else if (p0 == 1) {
            if (completedTournmentFragment == null)
                completedTournmentFragment = CompletedTournmentFragment()
            return completedTournmentFragment!!
        } else  {
            if (videoListFragment == null)
                videoListFragment = VideoListFragment()
            return videoListFragment!!
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    fun refreshRecycleAdapters() {
        completedTournmentFragment?.bindView()
        tournmentFragment?.bindView()
        videoListFragment?.bindView()
    }

    class TournmentFragment : Fragment() {

        private var tournmentListReclerView: RecyclerView? = null
        private var tournmentListRecyclerAdapter: TournmentListRecyclerAdapter? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.view_pager_fragment_tournment_list, container, false)
            if (tournmentListReclerView == null) tournmentListReclerView =
                    view.findViewById(R.id.tournment_list_recycle_view)
            tournmentListReclerView?.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
            tournmentListRecyclerAdapter = TournmentListRecyclerAdapter(this.context!!)
            tournmentListReclerView?.adapter = this.tournmentListRecyclerAdapter
            return view
        }

        fun bindView() {
            tournmentListRecyclerAdapter?.notifyDataSetChanged()
        }
    }

    class CompletedTournmentFragment : Fragment() {

        private var completedListRecyclerView: RecyclerView? = null
        private var completedListRecyclerAdapter: CompletedListRecyclerAdapter? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.view_pager_fragment_completed_list, container, false)
            if (completedListRecyclerView == null) completedListRecyclerView =
                    view.findViewById(R.id.completed_list_recycler_view)
            completedListRecyclerView?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            completedListRecyclerAdapter = CompletedListRecyclerAdapter(this.context!!)
            completedListRecyclerView?.adapter = this.completedListRecyclerAdapter
            return view
        }


        fun bindView() {
            completedListRecyclerAdapter?.notifyDataSetChanged()
        }
    }

    class VideoListFragment : Fragment() {

        private var videoListRecyclerView: RecyclerView? = null
        private var videoListRecyclerAdapter: VideoListRecyclerAdapter? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.view_pager_fragment_video_list, container, false)
            if (videoListRecyclerView == null) videoListRecyclerView = view.findViewById(R.id.video_list_recycler_view)
            videoListRecyclerView?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            videoListRecyclerAdapter = VideoListRecyclerAdapter(context!!)
            videoListRecyclerView?.adapter = this.videoListRecyclerAdapter
            return view
        }

        fun bindView() {
            videoListRecyclerAdapter?.notifyDataSetChanged()
        }
    }
}
