package com.example.myapplication.arena.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R

class ArenaFragment : Fragment() {

    private var homeViewPager: ViewPager? = null
    private var homeViewPagerAdapter: HomeViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_arena, container, false)
        homeViewPager = view.findViewById(R.id.home_view_pager)
        homeViewPagerAdapter = HomeViewPagerAdapter(activity!!.baseContext, this.childFragmentManager)
        homeViewPager?.offscreenPageLimit = 3
        homeViewPager?.adapter = this.homeViewPagerAdapter
        homeViewPager?.isSaveFromParentEnabled = true
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object {
        fun newInstance(): ArenaFragment = ArenaFragment()
    }

    override fun onResume() {
        super.onResume()
        homeViewPagerAdapter?.refreshRecycleAdapters()
    }

//    fun refreshAdpterData() {
//        homeViewPagerAdapter!!.notifyDataSetChanged()
//    }
}
