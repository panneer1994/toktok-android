package com.example.myapplication.arena.home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import extension.inflate

class CompletedListRecyclerAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return CompletedListViewHolder(p0.inflate(p1, false, this.context))
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.completed_list_view_holder
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is CompletedListRecyclerAdapter.CompletedListViewHolder) p0.bindView()
    }

    class CompletedListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        override fun onClick(v: View?) {

        }

        init {
            itemView.setOnClickListener(this)
        }

        fun bindView() {

        }
    }
}
